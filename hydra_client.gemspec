$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "hydra/client/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "hydra_client"
  s.version     = Hydra::Client::VERSION
  s.authors     = ["Hai Le"]
  s.email       = ["hailn@topica.edu.vn"]
  s.homepage    = "http://hydra.pedia.vn"
  s.summary     = "Client for Hydra - CAS."
  s.description = "We really don't know what will happen tomorrow."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.2.5"

  s.add_development_dependency "rack-cas", "~> 0"
end
