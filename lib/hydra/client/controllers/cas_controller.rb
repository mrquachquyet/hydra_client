require 'hydra/client/user'
require 'active_support/concern'

module Hydra
  module Client
    module Controllers
      module CasController
        extend ActiveSupport::Concern

        included do
          before_action :set_hydra_user
        end

        def hydra_authenticate!
          cas = request.session['cas']
          if cas.blank? || cas['user'].blank?
            render nothing: true, status: 401
          else
            set_hydra_user
          end
        end

        def hydra_user_signed_in?
          cas = request.session['cas']
          !(cas.blank? || cas['user'].blank?)
        end

        def set_hydra_user
          if hydra_user_signed_in? && !@hydra_user
            @hydra_user = Hydra::Client::User.new session
          end
        end
      end
    end
  end
end
