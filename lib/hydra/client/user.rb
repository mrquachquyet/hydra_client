module Hydra
  module Client
    class User

      attr_accessor :email, :name, :roles

      def initialize session
        cas = session['cas']
        if cas.blank? || cas['user'].blank?
          raise ArgumentError, "Try to create hydra user without cas info in session"
        end

        @email = cas['user']
        @name  = (cas['extra_attributes'] || {})['name']
        @roles = (cas['extra_attributes'] || {})['roles'] || []
      end
    end
  end
end