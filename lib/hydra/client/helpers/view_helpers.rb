module Hydra
  module Client
    module Helpers
      module ViewHelpers
        def cas_login_path
          callback = URI.encode_www_form_component URI.join(
            [request.protocol, request.host_with_port].join,
            Hydra::Client.config.login_callback
          ).to_s
          api = URI.join(Hydra::Client.config.server_url, '/login')
          "#{api}?service=#{callback}"
        end

        def cas_logout_path
          callback = URI.encode_www_form_component URI.join(
            [request.protocol, request.host_with_port].join,
            Hydra::Client.config.logout_callback
          ).to_s
          "/logout?service=#{callback}"
        end
      end
    end
  end
end