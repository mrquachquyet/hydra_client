module Hydra
  module Client
    class Railtie < Rails::Railtie
      config.hydra_client = ActiveSupport::OrderedOptions.new

      initializer 'hydra_client.initialize', before: 'rack_cas.initialize', group: :all do |app|
        hydra_config = Hydra::Client.config
        hydra_config.update config.hydra_client

        Hydra::Client::Configuration::CAS_SETTINGS.each do |key|
          value = hydra_config.method(key).call
          config.rack_cas.method(eval(":#{key.to_s}=")).call(value) unless value.nil?
        end

        ActiveSupport.on_load :action_view do
          require 'hydra/client/helpers/view_helpers'
          include Hydra::Client::Helpers::ViewHelpers
        end

        ActiveSupport.on_load(:action_controller) do
          require 'hydra/client/controllers/cas_controller'
          include Hydra::Client::Controllers::CasController
        end
      end
    end
  end
end
