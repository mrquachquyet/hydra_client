module Hydra
  module Client
    class Configuration
      HYDRA_SETTINGS = [:logout_callback, :login_callback]
      CAS_SETTINGS = [:fake, :server_url, :session_store, :exclude_path, :exclude_paths, :extra_attributes_filter, :verify_ssl_cert, :renew, :use_saml_validation]
      SETTINGS =  CAS_SETTINGS + HYDRA_SETTINGS

      SETTINGS.each do |setting|
        attr_accessor setting

        define_method "#{setting}?" do
          !(send(setting).nil? || send(setting) == [])
        end
      end

      def initialize
        @server_url = cas_server
        @login_callback = '/'
        @logout_callback = '/'
        @verify_ssl_cert = false
        @extra_attributes_filter = %w(name roles)
      end

      def cas_server
        Rails.env.production? ? 'http://hydra.pedia.vn' : 'http://localhost:5000'
      end

      def extra_attributes_filter
        Array(@extra_attributes_filter)
      end

      def update(settings_hash)
        settings_hash.each do |setting, value|
          unless SETTINGS.include? setting.to_sym
            raise ArgumentError, "invalid setting: #{setting}"
          end

          self.public_send "#{setting}=", value
        end
      end
    end
  end
end
