require 'hydra/client/configuration'
require 'hydra/client/railtie' if defined?(Rails)

module Hydra
  module Client
    def self.configure
      yield config
    end

    def self.config
      @config ||= Configuration.new
    end
  end
end